#ifndef _CONFIG_HPP
#define _CONFIG_HPP
#include "helpers.hpp"



namespace Config {
constexpr auto targetFps = 120;
constexpr auto frameTime = Helpers::calcMsPerFrame(targetFps);
constexpr auto animFrameTime = Helpers::calcMsPerFrame(targetFps);
}
#endif
