#ifndef _GAME_HPP
#define _GAME_HPP
#include "map.hpp"
#include "sprites.hpp"
#include "helpers.hpp"
#include "config.hpp"

#include <vector>
#include <memory>
#include <chrono>
#include <SFML/Graphics.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/awaitable.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/co_spawn.hpp>
#include <boost/asio/detached.hpp>



class Game {
    sf::RenderWindow& w;
    sf::Vector2u windowSize;

    Sprites sprites;
    Map map;

    sf::Texture bgTexture;
    sf::Sprite bgSprite;
    uint mapX = 0;

    bool ended = false;
    boost::asio::io_context& scheduler;

    boost::asio::awaitable<void> mapProgressor() {
        while (!ended) {
            // Proceed the clock
            co_await Helpers::co_sleep(scheduler, std::chrono::milliseconds(10));
            // Do some steps
            map.generate(scheduler, (++mapX) + windowSize.x + 100/*generate ahead*/);
        }
    }

    void renderBackground() {
        auto textureSize = bgTexture.getSize();
        auto shift = (mapX / 2) % textureSize.x;
        bgSprite.setTextureRect(sf::IntRect(-textureSize.x + shift, 0, windowSize.x + textureSize.x, windowSize.y));
        w.draw(bgSprite);
    }

    void renderForeground() {
        // Render map
        for (const auto& obj : map.objects) {
            auto sprite = obj->getSprite(sprites);
            if (obj->absolutePos) {
                sprite.setPosition(obj->position.x, obj->position.y);
            } else {
                sprite.setPosition(obj->position.x - mapX, obj->position.y);
            }
            w.draw(sprite);
        }
        // Render space ship
        auto spaceShipSprite = map.spaceShip->getSprite(sprites);
        spaceShipSprite.setPosition(map.spaceShip->position);
        w.draw(spaceShipSprite);
    }

    void render() {
        renderBackground();
        renderForeground();
    }

    void end() {
        ended = true;
        // Let everything do its final stuff
        render();
    }

    boost::asio::awaitable<void> mainLoop() {
        while (w.isOpen()) {
            auto now = std::chrono::steady_clock::now();
            // Get events
            sf::Event event;
            while (w.pollEvent(event)) {
                if (event.type == sf::Event::Closed) {
                    w.close();
                } else if (event.type == sf::Event::KeyPressed) {
                    if (event.key.code == sf::Keyboard::Escape) {
                        end();
                        co_return;
                    } else if (event.key.code == sf::Keyboard::Backspace) {
                        mapX = 0;
                    }
                }
            }

            // Render and display
            render();
            w.display();

            // Make sure to have target FPS
            co_await Helpers::co_sleep(scheduler, std::chrono::milliseconds(Config::frameTime));
        }
        end();
    }

public:
    Game(sf::RenderWindow& window, boost::asio::io_context& scheduler)
        : w(window),
          sprites("assets", "sheet.xml"),
          scheduler(scheduler) {
        map.spawnSpaceShip(scheduler);
        windowSize = w.getSize();

        // Initialize background rendering
        bgTexture.loadFromFile("assets/bg.png");
        bgTexture.setRepeated(true);
        bgSprite.setTexture(bgTexture);
    }

    auto run() {
        boost::asio::co_spawn(scheduler, mapProgressor(), boost::asio::detached);
        return mainLoop();
    }
};
#endif
