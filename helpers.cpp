#include "helpers.hpp"
#include "config.hpp"

#include <iostream>
#include <chrono>
#include <cmath>
#include <ctime>
#include <SFML/Graphics.hpp>
#include <boost/asio/awaitable.hpp>



namespace Helpers {
boost::asio::awaitable<void> shakeView(boost::asio::io_context& scheduler, sf::RenderWindow& window, int shakeDuration) {
    auto view = window.getView();
    auto initalCenter = view.getCenter();
    float elapsed = 0;
    float horizontalAmplitude = 2;
    float verticalAmplitude = 4;
    float horizontalFrequency = 0.1;
    float verticalFrequency = 0.1;
    while (shakeDuration > 0) {
        shakeDuration -= Config::animFrameTime;
        elapsed += Config::animFrameTime;
        sf::Vector2f newCenter;
        newCenter.y = initalCenter.y + verticalAmplitude * sin(elapsed * verticalFrequency);
        newCenter.x = initalCenter.x + horizontalAmplitude * sin(elapsed * horizontalFrequency);
        view.setCenter(newCenter);
        window.setView(view);
        co_await co_sleep(scheduler, std::chrono::milliseconds(Config::animFrameTime));
    }
    view.setCenter(initalCenter);
    window.setView(view);
}
}
