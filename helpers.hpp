#ifndef _HELPERS_HPP
#define _HELPERS_HPP
#include <SFML/Graphics.hpp>
#include <boost/asio/awaitable.hpp>
#include <boost/asio/use_awaitable.hpp>
#include <boost/asio/steady_timer.hpp>

#include <concepts>



namespace Helpers {
template<typename tuT>
boost::asio::awaitable<void> co_sleep(boost::asio::io_context& scheduler, tuT duration) {
    co_await boost::asio::steady_timer(scheduler, duration)
            .async_wait(boost::asio::use_awaitable);
}

constexpr auto calcMsPerFrame(std::integral auto fps) {
    return 1000 / fps;
}

boost::asio::awaitable<void> shakeView(boost::asio::io_context& scheduler, sf::RenderWindow& window, int shakeDuration = 250);
}
#endif
