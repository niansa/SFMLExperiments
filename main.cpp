﻿#include "game.hpp"
#include "menu.hpp"

#include <string_view>
#include <memory>
#include <vector>
#include <chrono>
#include <SFML/Graphics.hpp>
#include <boost/asio/io_context.hpp>



int main() {
    boost::asio::io_context scheduler;
    auto menu = new MainMenu(sf::VideoMode(800, 600), scheduler);
    menu->run();
    while (!scheduler.stopped()) {
        // Hacky but works - to be improved
        if (menu->getWindow()->hasFocus() || !menu->getWindow()->isOpen()) {
            scheduler.run_for(std::chrono::seconds(1));
        }
    }
    //scheduler.run();
}
