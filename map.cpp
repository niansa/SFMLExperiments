#include "map.hpp"

#include <random>
#include <boost/asio/co_spawn.hpp>
#include <boost/asio/detached.hpp>



Map::~Map() {
    spaceShip->ended = true;
    for (auto& obj : objects) {
        obj->ended = true;
    }
}

void Map::spawnObject(boost::asio::io_context& sched, std::shared_ptr<Object> o) {
    o->setup();
    boost::asio::co_spawn(sched, o->run(sched), boost::asio::detached);
    objects.push_back(o);
}

void Map::spawnSpaceShip(boost::asio::io_context& sched) {
    spaceShip = std::make_shared<SpaceShip>(*this);
    spaceShip->position = {100, 500};
    spaceShip->setup();
    boost::asio::co_spawn(sched, spaceShip->run(sched), boost::asio::detached);
}

void Map::spawnProjectile(boost::asio::io_context& sched) {
    auto projectile = std::make_shared<Projectile>(*this);
    projectile->position = spaceShip->position;
    spawnObject(sched, projectile);
}

void Map::spawnRock(boost::asio::io_context& sched, const sf::Vector2f& pos) {
    auto rock = std::make_shared<Rock>(*this);
    rock->position = pos;
    spawnObject(sched, rock);
}

void Map::generate(boost::asio::io_context& sched, uint generate_until) {
    // Create randomizer
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist6(0, 50000);
    // Generate
    while (generate_until > generated_until) {
        sf::Vector2f pos(generated_until, 0);
        // Iterate through Y
        for (uint y = 0; y != 500; pos.y = ++y) {
            switch (dist6(rng)) {
            case 0: spawnRock(sched, pos); break;
            }
        }
        generated_until += 2;
    }
}
