#ifndef _MAP_HPP
#define _MAP_HPP
#include "sprites.hpp"
#include "objects.hpp"

#include <string>
#include <vector>
#include <utility>
#include <memory>
#include <unordered_map>
#include <SFML/Graphics.hpp>
#include <boost/asio/io_context.hpp>



struct Map {
    std::vector<std::shared_ptr<Object>> objects;
    uint generated_until = 0;
    std::shared_ptr<SpaceShip> spaceShip;

    ~Map();
    void spawnObject(boost::asio::io_context& sched, std::shared_ptr<Object> o);
    void spawnSpaceShip(boost::asio::io_context& sched);
    void spawnProjectile(boost::asio::io_context& sched);
    void spawnRock(boost::asio::io_context& sched, const sf::Vector2f& pos);
    void generate(boost::asio::io_context&, uint generate_until);
};
#endif
