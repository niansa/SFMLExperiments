#include "menu.hpp"
#include "config.hpp"

#include <chrono>
#include <boost/asio/awaitable.hpp>
#include <boost/asio/io_context.hpp>



boost::asio::awaitable<void> MenuIntro::runIntro(boost::asio::io_context& scheduler) {
    uint introPart = 0;
    auto currEntry = entries.begin();
    // Run until animation is done
    while(!finished) {
        // Run current part of the intro
        switch (introPart) {
        case 0: {
            // Move header bar
            auto pos = headerBar.getPosition();
            pos.x += slidein_speed;
            if (pos.x >= 0) {
                pos.x = 0;
                introPart++;
            }
            headerBar.setPosition(pos);
        } break;
        case 1: {
            // Move current entry
            auto pos = currEntry->text.getPosition();
            pos.y += slidein_speed;
            currEntry->text.setPosition(pos);
            slidein += slidein_speed;
            if (slidein >= 0) {
                slidein = slidein_start;
                boost::asio::co_spawn(scheduler, Helpers::shakeView(scheduler, *w), boost::asio::detached);
                if (++currEntry == entries.end()) {
                    introPart++;
                }
            }
        } [[fallthrough]];
        case 2: {
            // Make header text visible
            // TODO: Sync up with animation speed
            auto color = headerText.getFillColor();
            if (++color.a == 0xff) {
                introPart++;
            }
            headerText.setFillColor(color);
        } break;
        default: finished = true;
        }
        // Proceed clock
        co_await Helpers::co_sleep(scheduler, std::chrono::milliseconds(Config::animFrameTime));
    }
}
