#ifndef _MENU_HPP
#define _MENU_HPP
#include "game.hpp"
#include "helpers.hpp"

#include <memory>
#include <vector>
#include <optional>
#include <functional>
#include <chrono>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/awaitable.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/co_spawn.hpp>
#include <boost/asio/detached.hpp>



class MenuEntry {
public:
    sf::Text text;
    using cbT = std::function<boost::asio::awaitable<void> ()>;
    cbT callback;

    MenuEntry(const sf::String& label, const sf::Font& font, cbT cb)
        : callback(cb) {
        text.setFont(font);
        text.setString(label);
        text.setCharacterSize(40);
        text.setFillColor(sf::Color::White);
        text.setOrigin(sf::Vector2f(text.getGlobalBounds().width / 2.f, text.getGlobalBounds().height / 2.f));
    }
};

struct MenuIntro {
    std::unique_ptr<sf::RenderWindow>& w;
    std::vector<MenuEntry>& entries;
    sf::Text& headerText;
    sf::Shape& headerBar;

    static constexpr float slidein_frametime = 50;
    static constexpr float slidein_speed = (Config::animFrameTime / slidein_frametime) * 100;
    static constexpr float slidein_start = -1000;
    float slidein = slidein_start;

    bool finished = false;

    boost::asio::awaitable<void> runIntro(boost::asio::io_context& scheduler);
};

class MainMenu {    
    friend MenuIntro;
    friend MenuEntry;

    std::unique_ptr<sf::RenderWindow> w;
    sf::VideoMode videoMode;
    sf::Sound player;

    std::vector<MenuEntry> entries;
    std::optional<decltype(entries)::iterator> hoveredEntry;

    sf::Font mainFont;
    sf::SoundBuffer pickSound, selectSound;
    sf::RectangleShape headerBar;
    sf::Text headerText;

    MenuIntro intro;

    boost::asio::io_context& scheduler;

    void unhoverEntry() {
        if (hoveredEntry.has_value()) {
            hoveredEntry.value()->text.setScale(1, 1);
            hoveredEntry.reset();
        }
    }
    void hoverEntry(decltype(entries)::iterator entry) {
        entry->text.setScale(1.25, 1.25);
        hoveredEntry = entry;
        player.setBuffer(pickSound);
        player.play();
    }

    void inputProc() {
        // Scale hovered "selected" entry
        if (w->hasFocus()) {
            auto mousepos = sf::Mouse::getPosition(*w);
            bool foundAny = false;
            for (auto it = entries.begin(); it != entries.end(); it++) {
                // Check if mouse cursor is on the entry
                if (it->text.getGlobalBounds().contains({float(mousepos.x), float(mousepos.y)})) {
                    // If so, hover it if not hovered already
                    if (!hoveredEntry.has_value() || it != hoveredEntry) {
                        // Unselect old entry
                        unhoverEntry();
                        // Select new entry
                        hoverEntry(it);
                    }
                    foundAny = true;
                    break;
                }
            }
            // If none were found, unhover currently hovered entry
            if (!foundAny) {
                unhoverEntry();
            }
        }
    }

    void render() {
        w->clear(sf::Color::Black);
        // Bar with Version
        w->draw(headerBar);
        w->draw(headerText);
        // Draw menu entries
        for (const auto& entry : entries) {
            w->draw(entry.text);
        }
    }

    boost::asio::awaitable<void> mainLoop() {
        // Run until window is closed
        while (w->isOpen()) {
            auto now = std::chrono::steady_clock::now();
            // Get events
            sf::Event event;
            while (w->pollEvent(event)) {
                if (event.type == sf::Event::Closed) {
                    w->close();
                } else if (event.type == sf::Event::KeyPressed) {
                    if (event.key.code == sf::Keyboard::Escape) {
                        w->close();
                    }
                } else if (event.type == sf::Event::MouseButtonReleased && hoveredEntry.has_value()) {
                    player.setBuffer(selectSound);
                    player.play();
                    co_await hoveredEntry.value()->callback();
                }
            }

            // Process input
            inputProc();

            // Render
            render();
            w->display();

            // Make sure to have target FPS
            co_await Helpers::co_sleep(scheduler, std::chrono::milliseconds(Config::frameTime));
        }
        // Stop intro
        intro.finished = true;
    }

public:
    MainMenu(const sf::VideoMode& videoMode, boost::asio::io_context& scheduler)
        : videoMode(videoMode),
          intro{w, entries, headerText, headerBar},
          scheduler(scheduler) {
        w = std::make_unique<sf::RenderWindow>(videoMode, "Test game", sf::Style::Titlebar | sf::Style::Close);
        mainFont.loadFromFile("assets/kenvector_future.ttf");
        pickSound.loadFromFile("assets/sounds/ui/pick.wav");
        selectSound.loadFromFile("assets/sounds/ui/selectA.wav");

        entries = {
            MenuEntry("Start game", mainFont, [this, videoMode] () -> boost::asio::awaitable<void> {
                co_await Game(*w, this->scheduler).run();
            }),
            MenuEntry("Credits", mainFont, [this] () -> boost::asio::awaitable<void> {
                co_return;
            }),
            MenuEntry("Exit", mainFont, [this] () -> boost::asio::awaitable<void> {
                w->close();
                co_return;
            })
        };

        float y = videoMode.height / 2;
        for (auto& entry : entries) {
            entry.text.setPosition(videoMode.width / 2, y + intro.slidein_start);
            y += entry.text.getGlobalBounds().height + 20;
        }

        headerBar.setPosition(intro.slidein_start, (videoMode.height / 4) - headerBar.getSize().y);
        headerBar.setFillColor(sf::Color::Red);
        headerBar.setSize({float(videoMode.width), 100});
        headerText.setFont(mainFont);
        headerText.setStyle(sf::Text::Bold);
        headerText.setCharacterSize(60);
        headerText.setFillColor(sf::Color(0xff, 0xff, 0xff, 0));
        headerText.setString("Dev Version");
        headerText.setPosition((headerBar.getSize().x - headerText.getGlobalBounds().width) / 2,
                                headerBar.getPosition().y);
    }

    void run() {
        boost::asio::co_spawn(scheduler, intro.runIntro(scheduler), boost::asio::detached);
        boost::asio::co_spawn(scheduler, mainLoop(), boost::asio::detached);
    }

    const auto& getWindow() {
        return w;
    }
};
#endif
