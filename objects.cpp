#include "objects.hpp"
#include "helpers.hpp"

#include <algorithm>
#include <boost/asio/awaitable.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/co_spawn.hpp>
#include <boost/asio/detached.hpp>



sf::Sprite& Object::getSprite(Sprites& sprites) {
    if (!spriteLoaded) {
        sprite = sprites.get(sprite_name);
        processSprite();
    }
    spriteLoaded = true;
    return sprite;
}

void Object::disappear() {
    if (!ended) {
        std::erase(map.objects, shared_from_this());
        ended = true;
    }
}

boost::asio::awaitable<void> MovingObject::run(boost::asio::io_context& sched) {
    auto me = shared_from_this();
    boost::asio::co_spawn(sched, move(sched), boost::asio::detached);
    while (!ended) {
        // Collision stuff
        if (collision) {
            // Check if colliding object has disappeared ("ended")
            if (collidesWith) {
                if (collidesWith->ended) {
                    collidesWith = nullptr;
                }
            }
            // Check for new collision
            for (const auto& o : map.objects) {
                if (o.get() != this && o->collision && o->spriteLoaded && o->sprite.getGlobalBounds().intersects(this->sprite.getGlobalBounds())) {
                    collidesWith = o;
                    break;
                }
            }
        }
        // Move
        position.x += velocity.x;
        position.y += velocity.y;
        // Interval
        co_await Helpers::co_sleep(sched, tick_interval);
    }
}


void Projectile::setup() {
    collision = true;
    absolutePos = true;
    velocity.x = 10;
    sprite_name = "laserRed01";
}

boost::asio::awaitable<void> Projectile::move(boost::asio::io_context& sched) {
    // Explode on collision
    if (collidesWith) {
        collidesWith->disappear();
        this->disappear();
        co_return;
    }
    // Make object expire at some point
    if (++steps == 20) {
        disappear();
        co_return;
    }
    // Delay next tick
    co_await Helpers::co_sleep(sched, std::chrono::milliseconds(200));
}

void Projectile::processSprite() {
    sprite.rotate(90);
    auto bounds = sprite.getGlobalBounds();
    sprite.setOrigin(bounds.height/2, bounds.width);
}


void SpaceShip::setup() {
    collision = true;
    sprite_name = "playerShip2_red";
}

boost::asio::awaitable<void> SpaceShip::move(boost::asio::io_context& sched) {
    auto me = shared_from_this();
    while (!ended) {
        velocity = {0, 0};
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
            velocity.y -= 5;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
            velocity.y += 5;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
            velocity.x -= 5;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
            velocity.x += 5;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
            map.spawnProjectile(sched);
        }
        co_await Helpers::co_sleep(sched, std::chrono::milliseconds(60));
    }
}

void SpaceShip::processSprite() {
    sprite.rotate(90);
    auto bounds = sprite.getGlobalBounds();
    sprite.setOrigin(bounds.height/2, bounds.width/2);
}


void Rock::setup() {
    collision = true;
    sprite_name = "meteorBrown_big1";
}

void Rock::processSprite() {
    sprite.rotate(90);
    auto bounds = sprite.getGlobalBounds();
    sprite.setOrigin(bounds.height/2, bounds.width/2);
}
