struct Object;
struct Projectile;
struct SpaceShip;

#ifndef _OBJECTS_HPP
#define _OBJECTS_HPP
#include "sprites.hpp"
#include "map.hpp"

#include <chrono>
#include <memory>
#include <SFML/Graphics.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/awaitable.hpp>

struct Map;



struct Object : public std::enable_shared_from_this<Object> {
    Map& map;
    sf::Sprite sprite;
    std::string sprite_name;
    sf::Vector2f position;
    bool spriteLoaded = false;
    bool collision = false;
    bool ended = false;
    bool absolutePos = false;

    Object(Map& map) : map(map) {}

    sf::Sprite& getSprite(Sprites& sprites);
    virtual void setup() {}
    void disappear();
    virtual boost::asio::awaitable<void> run(boost::asio::io_context&) {co_return;}
    virtual void processSprite() {}
};

struct MovingObject : public Object {
    sf::Vector2f velocity = {0, 0};
    std::chrono::milliseconds tick_interval = std::chrono::milliseconds(15);
    std::shared_ptr<Object> collidesWith = nullptr;

    using Object::Object;

    virtual boost::asio::awaitable<void> run(boost::asio::io_context& sched) override;
    virtual boost::asio::awaitable<void> move(boost::asio::io_context&) = 0;
};

struct Projectile final : public MovingObject {
    int steps = 0;

    using MovingObject::MovingObject;

    virtual void setup() override;
    virtual boost::asio::awaitable<void> move(boost::asio::io_context& sched) override;
    virtual void processSprite() override;
};

struct SpaceShip final : public MovingObject {
    using MovingObject::MovingObject;

    virtual void setup() override;
    virtual boost::asio::awaitable<void> move(boost::asio::io_context& sched) override;
    virtual void processSprite() override;
};

struct Rock final : public Object {
    using Object::Object;

    virtual void setup() override;
    virtual void processSprite() override;
};
#endif
