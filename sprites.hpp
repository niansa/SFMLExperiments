#ifndef _SPRITES_HPP
#define _SPRITES_HPP
#include "map.hpp"
#include "pugixml/pugixml.hpp"

#include <unordered_map>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <SFML/Graphics.hpp>



class Sprites {
    sf::Texture texture;
    std::unordered_map<std::string, sf::IntRect> sprites;

public:
    Sprites(const std::filesystem::path& path, const std::string& file) {
        // Read and parse file
        pugi::xml_document doc;
        auto result = doc.load_file((path/file).c_str());
        if (!result) {
            throw std::runtime_error("Failed to read sprite info file");
        }
        // Go trough it
        auto root = doc.child("TextureAtlas");
        this->texture.loadFromFile(path/(root.attribute("imagePath").as_string()));
        for (const auto child : root.children("SubTexture")) {
            sprites[child.attribute("name").as_string()] = sf::IntRect(
                    child.attribute("x").as_int(),
                    child.attribute("y").as_int(),
                    child.attribute("width").as_int(),
                    child.attribute("height").as_int()
            );
        }
    }

    sf::Sprite get(const std::string& name) {
        sf::Sprite sprite;
        sprite.setTexture(texture);
        sprite.setTextureRect(sprites[name]);
        return sprite;
    }
};
#endif
